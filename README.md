# README #

![Alt text](https://bytebucket.org/david_ferrer/symfony/raw/042ead2f8642730a1b5661d73aa7d925dd0c9912/1.png)
[Manual - DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04) 

### Problemes ###

* Fitxer de configuració de la codificació del MYSQL:
* El manual indica aquest arxiu [/etc/mysql/my.cnf]

* Nosaltres com utilitzem php7 és la següent ruta [/etc/mysql/mysql.conf.d/mysqld.cnf]
* Indicar el fus horari del PHP
El manual indica aquest arxiu:
	/etc/php5/fpm/php.ini
Nosaltres com utilitzem php7 és la següent ruta:
	/etc/php/7.0/fpm/php.ini
	
* Petita modificació arxiu del VH:
El manual indica afegir aquesta línia:
	Order Allow,Deny
En el nostre servidor s'ha de treure

* He tingut un problema de permisos i calia un pas més:
Calia que afegís la directiva per a poder tenir permís d'accés
	Require all granted